import aaronsSQL.Model.Friendslist;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

@WebServlet(name = "RemoveFriendServlet", value = "/RemoveFriend")
public class RemoveFriendServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In the get FRemove " + request.getQueryString() + ", " + request.getAttribute("FriendName"));
        System.out.println("checking frnds req " + request.getAttribute("frndsLst"));
        String username = request.getQueryString().split("&")[0].split("=")[1];
        String friendName = request.getQueryString().split("&")[1].split("=")[1];
        removeFriend(username, friendName, request, response);
    }

    private void removeFriend(String username, String friendName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration config = new Configuration();
        config.configure();
        // local SessionFactory been created
        SessionFactory sessionFactory = config.buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();

        session.getTransaction().begin();

        int uID = -1;
        int frienduID = -1;
        // get uID of logged in user
        String qry = "SELECT uID FROM users u where u.userName = :value1";
        Query q = session.createNativeQuery(qry)
                .setParameter("value1", username);
        uID = Integer.parseInt(q.getSingleResult().toString());

        // get uID of friend to be removed
        String friendIDQry = "SELECT uID FROM users u where u.userName = :value1";
        q = session.createNativeQuery(friendIDQry)
                .setParameter("value1", friendName);
        frienduID = Integer.parseInt(q.getSingleResult().toString());


        if (uID != 0 && frienduID != 0) {
            try {

                System.out.println("userLogged in info: " + username + ", " + uID );
                System.out.println("friend in info: " + friendName + ", " + frienduID);
                // get ID association for user in friendsList
                int usrFLstID = -1;
                String friendListUIDQry = "SELECT ID FROM friendslist f where f.uID = " + uID + " AND f.friendName =\"" + friendName +"\"" ;
                q = session.createNativeQuery(friendListUIDQry);
                usrFLstID = Integer.parseInt(q.getSingleResult().toString());

                // get ID association for friend in friendsList
                int frndFLstID = -1;
                String friendListFIDQry = "SELECT ID FROM friendslist f where f.uID = " + frienduID + " AND f.friendName =\"" + username + "\"";
                q = session.createNativeQuery(friendListFIDQry);
                frndFLstID = Integer.parseInt(q.getSingleResult().toString());

                if(usrFLstID != 0 && frndFLstID != 0) {

                    System.out.println("userLogged in info for FriendListID: " + usrFLstID);
                    System.out.println("friend in info for FriendListID: " + frndFLstID);
                    // Create two entities to remove each friend with one another

/*                    Query qury = session.createNativeQuery("DELETE FROM friendslist WHERE ID = "+ usrFLstID);
                    qury.executeUpdate();
                    Query qury2 = session.createNativeQuery("DELETE FROM friendslist WHERE ID = "+ frndFLstID);
                    qury2.executeUpdate();
                    session.getTransaction().commit();*/
                    removeFriend(username, uID, usrFLstID);
                    removeFriend(friendName, frienduID, frndFLstID);

                    // Redirect back to create friends page
                    request.setAttribute("friendAddStatus", "Friend was added successfully.");
                    request.setAttribute("userName", username);
                    RequestDispatcher rd =
                            getServletContext().getRequestDispatcher("/FriendsList");
                    rd.forward(request, response);
                }

            }catch(Exception e){
                System.out.println("Error while adding the friend" + e);
                request.setAttribute("userName", username);
                request.setAttribute("friendRemovedStatus", "failed to add");
                RequestDispatcher rd =
                        getServletContext().getRequestDispatcher("/FriendsList");
                rd.forward(request, response);
            }
        } else {
            request.setAttribute("friendAddStatus", "failed to add");
            request.setAttribute("userName", username);
            RequestDispatcher rd =
                    getServletContext().getRequestDispatcher("/FriendsList");
            rd.forward(request, response);
        }
    }

    private void removeFriend(String username, int uID, int usrFLstID) {
        Configuration config2 = new Configuration();
        config2.configure();
        // local SessionFactory been created
        SessionFactory sessionFactory2 = config2.buildSessionFactory();
        Session session2 = sessionFactory2.getCurrentSession();
        session2.getTransaction().begin();

        Friendslist tmp = new Friendslist();
        tmp.setFriendName(username);
        tmp.setuId(uID);
        tmp.setID(usrFLstID);
        session2.remove(tmp);
        session2.delete(tmp);
        session2.getTransaction().commit();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In the post FRemove " + request.getPathInfo() + ", " + request.getAttribute("FriendName"));
    }
}
