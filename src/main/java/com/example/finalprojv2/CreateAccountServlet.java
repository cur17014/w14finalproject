package com.example.finalprojv2;

import aaronsSQL.Model.Users;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/CreateNewUser") //urlPatters = "/hello"
public class CreateAccountServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        resp.setContentType("text/html");

        if(createNewPerson(userName, password)) {
            // Redirect back to create friends page
            req.setAttribute("accCreationStatus", "Account was created!");
            RequestDispatcher rd =
                    getServletContext().getRequestDispatcher("/index.jsp");
            rd.forward(req, resp);
            resp.sendRedirect("/index.jsp");
        }else{
            // Redirect back to create friends page
            req.setAttribute("accCreationStatus", "Account failed to be created!");
            RequestDispatcher rd =
                    getServletContext().getRequestDispatcher("/index.jsp");
            rd.forward(req, resp);
            resp.sendRedirect("/index.jsp");
        }
    }

    public void destroy() {
    }

    private boolean createNewPerson(String userName,String password ){
        Users temp = null;

        try {
            temp = new Users(userName, password);

            Configuration config = new Configuration();
            config.configure();
            // local SessionFactory bean created
            SessionFactory sessionFactory = config.buildSessionFactory();
            Session session = sessionFactory.getCurrentSession();
            session.getTransaction().begin();
            session.save(temp);
            session.getTransaction().commit();
            return true;
        }catch(Exception e){
            System.out.println("Failed to add user to the database. " + e);
            return false;
        }
    }
}