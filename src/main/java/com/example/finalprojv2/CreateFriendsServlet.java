package com.example.finalprojv2;

import aaronsSQL.Model.Friendslist;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "CreateFriendsServlet", value = "/CreateFriends")
public class CreateFriendsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Redirect to create friends page
        String userName = request.getParameter("username").toString();
        request.setAttribute("un", userName);
        RequestDispatcher rd =
                getServletContext().getRequestDispatcher("/addNewFriend.jsp");
        rd.forward(request, response);
        response.sendRedirect("/addNewFriend.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("username").toString();
        addFriend(userName, request, response);
    }

    private void addFriend(String userName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration config = new Configuration();
        config.configure();
        // local SessionFactory been created
        SessionFactory sessionFactory = config.buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();

        session.getTransaction().begin();
        String newFriendName = request.getParameter("friendName");

        System.out.println("Friend name is: " + newFriendName);
        try {
            int uID = -1;
            int frienduID = -1;
            // get uID of logged in user
            String qry = "SELECT uID FROM users u where u.userName = :value1";
            Query q = session.createNativeQuery(qry)
                    .setParameter("value1", userName);
            uID = Integer.parseInt(q.getSingleResult().toString());

            // get uID of Friend
            String friendIDQry = "SELECT uID FROM users u where u.userName = :value1";
            q = session.createNativeQuery(friendIDQry)
                    .setParameter("value1", newFriendName);
            frienduID = Integer.parseInt(q.getSingleResult().toString());


            if (uID != 0 && frienduID != 0) {
                System.out.println("uID: " + uID + ", your name: " + userName + "\n friendUID: " + frienduID + ", friend name: " + newFriendName);
                // Add Friend

                // Create both entities to add associate each friend with one another
                Friendslist friendslist = new Friendslist();
                friendslist.setFriendName(newFriendName);
                friendslist.setuId(uID);
                // Save
                session.save(friendslist);
                // Commit
                session.getTransaction().commit();

                Configuration config2 = new Configuration();
                config2.configure();
                // local SessionFactory been created
                SessionFactory sessionFactory2 = config2.buildSessionFactory();
                Session session2 = sessionFactory2.getCurrentSession();

                session2.getTransaction().begin();

                Friendslist friendslist1 = new Friendslist();
                friendslist1.setFriendName(userName);
                friendslist1.setuId(frienduID);
                // Save
                session2.save(friendslist1);
                //commit
                session2.getTransaction().commit();

                // Redirect back to create friends page
                request.setAttribute("friendAddStatus", "true");
                RequestDispatcher rd =
                        getServletContext().getRequestDispatcher("/addNewFriend.jsp");
                rd.forward(request, response);
            } else {
                request.setAttribute("friendAddStatus", "failed to add");
                RequestDispatcher rd =
                        getServletContext().getRequestDispatcher("/addNewFriend.jsp");
                rd.forward(request, response);
            }
        } catch (Exception e) {
            System.out.println("Error while adding the friend" + e);
            request.setAttribute("friendAddStatus", "failed to add");
            RequestDispatcher rd =
                    getServletContext().getRequestDispatcher("/addNewFriend.jsp");
            rd.forward(request, response);
        }
    }
}
