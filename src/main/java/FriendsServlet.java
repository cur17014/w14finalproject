import aaronsSQL.Model.Friendslist;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "FriendsServlet", value = "/FriendsList")
public class FriendsServlet extends HttpServlet {
    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String userName =request.getParameter("userName");
        if(request.getParameter("userName") == null) {
            userName = request.getAttribute("userName").toString();
        }
        String password = findPassword(userName);

        response.setContentType("text/html");

        if (findAndDisplayFriends(userName, password, response, request)) {
            request.setAttribute("foundFriends", "Friend was added successfully.");
        } else {
            request.setAttribute("foundFriends", "Error while adding friends");
        }
    }

    private String findPassword(String userName) {
        Configuration config = new Configuration();
        config.configure();
        // local SessionFactory been created
        SessionFactory sessionFactory = config.buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();

        session.getTransaction().begin();

        String pw = "";
        String qry = "SELECT password FROM users u where u.userName = :value1";
        Query q = session.createNativeQuery(qry)
                .setParameter("value1", userName);
        pw = q.getSingleResult().toString();
        return pw;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("In the get for friendsList: " + req.getParameter("userName") + ", " + req.getQueryString());

        String userName = req.getParameter("userName");
        String password = req.getParameter("password");

        if (findAndDisplayFriends(userName, password, resp, req)) {
            req.setAttribute("foundFriends", "Friend was added successfully.");
        } else {
            req.setAttribute("foundFriends", "Error while adding friends");
        }
    }

    private boolean findAndDisplayFriends(String userName, String password, HttpServletResponse resp, HttpServletRequest req) throws ServletException, IOException {

        Configuration config = new Configuration();
        config.configure();
        // local SessionFactory bean created
        SessionFactory sessionFactory = config.buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();

        session.getTransaction().begin();
        int id = -1;
        String qry = "SELECT uID FROM users u where u.userName = :value1 AND u.password = :value2";
        Query q = session.createNativeQuery(qry)
                .setParameter("value1", userName).setParameter("value2", password);
        try{
        id = Integer.parseInt(q.getSingleResult().toString());
        } catch(Exception e){
            // Unable to login, go to back main page
            req.setAttribute("loginStatus", "false");
            RequestDispatcher rd =
                    getServletContext().getRequestDispatcher("/");
            rd.forward(req, resp);
        }
        if (id != 0) {
           List<Friendslist> fl = session.createNativeQuery("SELECT friendName FROM friendslist t WHERE t.uID = :value1")
                    .setParameter("value1", id).getResultList();
            displayFriends(fl, userName, resp, req);
            return true;
        } else {
            return false;
        }
    }

    private void displayFriends(List<Friendslist> fl, String userName, HttpServletResponse resp, HttpServletRequest req) {
        PrintWriter out = null;
        try {
            out = resp.getWriter();
            resp.setContentType("text/html");
            req.setAttribute("friends", fl);
            req.setAttribute("usrName", userName);

            RequestDispatcher rd =
                    getServletContext().getRequestDispatcher("/DisplayFriends.jsp");
            rd.forward(req, resp);
            resp.sendRedirect("/DisplayFriends.jsp");

        }catch(Exception e){
            String message = "<html><body> <h1> Error occurred while attempting to display friends </h1> " +
                    "</body></html>";
            if(out == null){
                try {
                    out = resp.getWriter();
                }catch(IOException io){
                    System.out.println("Error occurred while attempting to display friends and while creating the out writer!");
                }
            }else {
                out.println(message);
            }
        }

    }
    public void destroy () {
    }
}
