package aaronsSQL.Model;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Users {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "uID")
    private int uId;
    @Basic
    @Column(name = "userName")
    private String userName;
    @Basic
    @Column(name = "password")
    private String password;

    public Users() {}

    public Users(String username, String password) {
        this.userName = username;
        this.password = password;
    }

/*    @OneToMany(mappedBy = "usersByUId")
    private Collection<Friendslist> friendslistsByUId;*/

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Users users = (Users) o;

        if (uId != users.uId) return false;
        if (userName != null ? !userName.equals(users.userName) : users.userName != null) return false;
        if (password != null ? !password.equals(users.password) : users.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uId;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

/*    public Collection<Friendslist> getFriendslistsByUId() {
        return friendslistsByUId;
    }

    public void setFriendslistsByUId(Collection<Friendslist> friendslistsByUId) {
        this.friendslistsByUId = friendslistsByUId;
    }*/
}
