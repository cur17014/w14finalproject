package aaronsSQL.Model;

import javax.persistence.*;

@Entity
public class Friendslist {

    @Basic
    @Column(name = "uID", updatable = false)
    private int uId;

    @Basic
    @Column(name = "friendName")
    private String friendName;
/*    @ManyToOne
    @JoinColumn(name = "uID", referencedColumnName = "uID", nullable = false)
    private Users usersByUId;*/

    @Id
    @Column(name = "ID")
    private int ID;

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Friendslist that = (Friendslist) o;

        if (uId != that.uId) return false;
        if (friendName != null ? !friendName.equals(that.friendName) : that.friendName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uId;
        result = 31 * result + (friendName != null ? friendName.hashCode() : 0);
        return result;
    }

 /*   public Users getUsersByUId() {
        return usersByUId;
    }

    public void setUsersByUId(Users usersByUId) {
        this.usersByUId = usersByUId;
    }*/

    public void setID(int id) {
        this.ID = id;
    }

    public int getID() {
        return ID;
    }
}
