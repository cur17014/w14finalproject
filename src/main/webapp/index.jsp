<!-- Latest compiled and minified CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Friends List Website/App</title>
</head>
<link rel="stylesheet" href="styles.css">
<body class="container bg-dark text-white">
<h1 ><%= "Welcome!" %>
</h1>
<br/>

<% if (request.getAttribute("accCreationStatus") == "Account was created!") { %>
    <script>alert("Account has been created, please proceed to login!")</script>
<% }else if(request.getAttribute("accCreationStatus") == "Account failed to be created!") { %>
    <script>alert("Account failed to be created, please try again.")</script>
<% } %>
<p >Please login with your ID and password or create a user <a href="CreateAccount.jsp">here</a></p>
<form name="loginForm" action="./FriendsList" method="POST" >
    <table>
        <tr>
            <td >Username:</td>
            <td><input type="text" name="userName"/></td>
        </tr>
        <tr>
            <td >Password:</td>
            <td><input type="text" name="password"/></td>
        </tr>
        <th><input type="submit"  class="btn btn-secondary" value="Submit" name="find"/></th>
        <th><input type="reset" class="btn btn-secondary" value="Reset" name="reset" /></th>
    </table>
</form>

</body>
<% if (request.getAttribute("loginStatus") == "false") { %>
<script>alert("Error logging in.")</script>
<% } %>
</html>