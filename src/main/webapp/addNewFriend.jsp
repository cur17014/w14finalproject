<%--
  Created by IntelliJ IDEA.
  User: Aaron
  Date: 12/12/2021
  Time: 4:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
<html>
<head>
    <meta charset="UTF-8">
    <title>Add new friend page</title>
</head>
<body class="container bg-dark text-white">
<p>
<h1>Enter the name a friend to add here</h1>
<br/>
return to your friends list page <a href="./FriendsList?userName=<%=request.getParameter("username")%>">here</a>
<form name="newFriendForm" action="./CreateFriends?username=<%=request.getParameter("username")%> " method="POST" >
    <table>
        <tr>
            <td>Friend Name:</td>
            <td><input type="text" name="friendName"/></td>
            <td><input hidden type="text" name="username" value="<%=request.getParameter("username")%>"/></td>
        </tr>
        <th><input type="submit" class="btn btn-secondary" value="Submit" name="find"/></th>
        <th><input type="reset" class="btn btn-secondary" value="Reset" name="reset" /></th>
    </table>
</form>
</p>
<% if(request.getAttribute("friendAddStatus") == "failed to add"){ %>
<script> alert("Unable to add the requested friend! Please try again.");  </script>
<% } else if(request.getAttribute("friendAddStatus") == "true") { %>
<script> alert( "Friend was added successfully" );  </script>
<% } request.setAttribute("friendAddStatus", ""); %>
</body>
</html>
