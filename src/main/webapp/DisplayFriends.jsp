<%@page import="aaronsSQL.Model.Friendslist"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Student List</title>
</head>
<body class="container bg-dark text-white">
<h1>Displaying Student List</h1>
<table class="table table-dark">
    <thead>
    <tr>
        <th scope="col">Friends Name</th>
        <%-- placeholder for 'x' icon to remove a friend--%>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <%-- List comes back as a JSON object. Here we cast it back to a list of type string--%>
    <%List<String> std =
            (List<String>) request.getAttribute("friends");
    request.setAttribute("frndsLst", request.getAttribute("friends"));
        %>
    <% for(String s : std){ System.out.println(s);
    %> <tr scope="row">
        <td> <%=s%></td>
        <td title="Remove Friend">
            <a href='./RemoveFriend?username=<%=request.getAttribute("usrName")%>&FriendName=<%=s%>'>
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="white" class="bi bi-x" viewBox="0 0 16 16">
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
            </svg>
            </a>
        </td>
    </tr>
    <% } %>
    </tbody>

</table>

<button type="button" onClick="window.location.href='./CreateFriends?username=<%=request.getAttribute("usrName")%>' "
        class="btn btn-primary">Add a new friend by clicking here</button>
<p id="generateForm"></p>

</body>
</html>