<%--
  Created by IntelliJ IDEA.
  User: Aaron
  Date: 12/9/2021
  Time: 4:17 PM
  To change this template use File | Settings | File Templates.
--%>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Create a new user</title>
</head>
<body class="container bg-dark text-white">
<h1><%= "Create your account by filling out the information below" %>
</h1>
<br/>

<form name="bmiForm" action="./CreateNewUser" method="POST" >
    <table>
        <tr>
            <td>Create a username:</td>
            <td><input type="text" name="userName"/></td>
        </tr>
        <tr>
            <td>Create a password:</td>
            <td><input type="text" name="password"/></td>
        </tr>
        <th><input class="btn btn-secondary" type="submit" value="Submit" name="find"/></th>
        <th><input class="btn btn-secondary" type="reset" value="Reset" name="reset" /></th>
    </table>
</form>


</body>
</html>
